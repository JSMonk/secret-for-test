const GoogleSpreadsheet = require('google-spreadsheet');
let { exec } = require('child_process');
const { promisify } = require('util');

exec = promisify(exec);

const credentials = require("./credentials.json");

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const command = `curl 'https://uc75acaff354bfdf5121a2be9269.dl.dropboxusercontent.com/cd/0/get/Aq_e9i5xnyeQCjfubUKmmkP3DbEP3HXZBIf51JKfoH8Z0pmiJhXFnnkHgTro5Fhorc_-v9vqzcY5aOXzTdDJeEmomtXH1yjBg2VGLACHEjUBZ71s7hx6-XMuukOXiRlsazM/file?dl=1' -H 'authority: uc75acaff354bfdf5121a2be9269.dl.dropboxusercontent.com' -H 'upgrade-insecure-requests: 1' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36' -H 'sec-fetch-mode: nested-navigate' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'sec-fetch-site: cross-site' -H 'referer: https://www.dropbox.com/' -H 'accept-encoding: gzip' -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7' --compressed -s -w "%{time_starttransfer} %{time_total}" -o /dev/null`;

(async () => {
  const results = [];
  const doc = new GoogleSpreadsheet("1UNguV6uDkBhzAXDi4zYt1WMDCrTHRGT7KI_gPzV30FI");
  await promisify(doc.useServiceAccountAuth)(credentials);
  const info = await promisify(doc.getInfo)();
  const sheet = info.worksheets[0];
  const cells = await promisify(sheet.getCells)({
    'min-row': 1,
    'max-row': 101,
    'min-col': 5,
    'max-col': 6,
  });
  for (let i = 0; i < 101; i++) {
     console.info(`Starting ${i} request`);
     const {stdout} = await exec(command);
     console.info(`Got ${i} request`);
     const [ttfb, total] = stdout.split(" ").map(Number);
    debugger;
    cells[i * 2].value = ttfb;
    cells[i * 2].save();
    console.info(`Uploaded ${i}`);
    cells[i * 2 + 1].value = total;
    cells[i * 2 + 1].save();
    console.info(`Uploaded ${i + 1}`);
     await sleep(100);
  }
})();
